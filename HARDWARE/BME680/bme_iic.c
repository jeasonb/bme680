#include "mybme680.h"
#include "delay.h"
#include "bme680_defs.h"
#include "bsec_datatypes.h"
#include "bsec_interface.h"

/* ????  ??????????
SPI_CS      PD0    GPIO
SPI_MISO    PC11   SPI3_MISO 
SPI_MOSI    PC12   SPI3_MOSI
SPI_CLK     PC10   SPI3_SCK

BME_IIC_SDA     PC12
BME_IIC_SCL     PC10
BME_IIC_ADDR    PC11     ????? 1
CSB             PD0      ????
???  VCC_MCU  
*/
#define BME_IIC_ADDR   (0xee)
#define UINT16_C(x)     (x)

//static void BME_IIC_Init(void);                      
static void BME_IIC_Start(void);                     
static void BME_IIC_Stop(void);                      
static void BME_IIC_Send_Byte(u8 txd);               
static u8   BME_IIC_Read_Byte(unsigned char ack);                   
static u8   BME_IIC_Wait_Ack(void);                  
static void BME_IIC_Ack(void);                       
static void BME_IIC_NAck(void);  

struct bme680_dev              gas_sensor;   //  设备结构体
struct bme680_field_data     filed_struct;
struct bme680_calib_data    calib_struct;
struct bme680_tph_sett        tph_sett_struct;
struct bme680_gas_sett       gas_sett_struct;
struct bme680_field_data     field_data_struct;

void bme_delay_ms(uint32_t period)
{
  delay_ms((u16)period);  
}
void my_bme_init(void)
{
  uint8_t set_required_settings;
  u8 rslt;
  BME680_Init();    // 单片机侧的  IO初始化
  bme680_soft_reset(&gas_sensor);  // 软复位一次

   gas_sensor.delay_ms        = bme_delay_ms;       //  延时函数的函数指针
   gas_sensor.read                = bme680_iic_read;   //  数据读取函数的指针 
   gas_sensor.write               = bme680_iic_write;   // 写数据的指针
   gas_sensor.intf                  = BME680_I2C_INTF; // 通讯方式的选择
   gas_sensor.power_mode    = BME680_FORCED_MODE;  // 读取方式 ？？？  这里不是很懂
   gas_sensor.chip_id             = BME680_CHIP_ID;  // 芯片ID  不知道啥用
  gas_sensor.dev_id              =BME680_I2C_ADDR_SECONDARY;

  gas_sensor.calib                 = calib_struct;         // 这是三个数据结构体的指针
  gas_sensor.tph_sett            = tph_sett_struct;
  gas_sensor.gas_sett           = gas_sett_struct;
  
  gas_sensor.tph_sett.os_hum   = BME680_OS_1X;  // 这里是  数据结构体下的 过采样率
  gas_sensor.tph_sett.os_temp  = BME680_OS_1X;
  gas_sensor.tph_sett.os_pres   = BME680_OS_1X;
  gas_sensor.tph_sett.filter         = BME680_FILTER_SIZE_1;
  //  还有一个空气质量的结构体指针我没解决  改日在研究
// /* Set the remaining gas sensor settings and link the heating profile */
//  gas_sensor.gas_sett.run_gas = BME680_ENABLE_GAS_MEAS;
//  /* Create a ramp heat waveform in 3 steps */
//  gas_sensor.gas_sett.heatr_temp = 320; /* degree Celsius */
//  gas_sensor.gas_sett.heatr_dur = 150; /* milliseconds */  
  gas_sensor.power_mode = BME680_FORCED_MODE;
  /* Set the required sensor settings needed */
	set_required_settings = (BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL | BME680_FILTER_SEL| BME680_GAS_SENSOR_SEL);

   rslt =  bme680_init(&gas_sensor);                           // 调用初始化函数
   if(rslt)printf("result of bme680_init() is %d",rslt);    // 有问题的话输出调试信息

/* Set the desired sensor configuration */
	 rslt =  bme680_set_sensor_settings(set_required_settings,&gas_sensor);
   if(rslt)printf("result of bme680_set_sensor_settings() is %d",rslt);

	/* Set the power mode */
	rslt =  bme680_set_sensor_mode(&gas_sensor);
  if(rslt)printf("result of bme680_set_sensor_mode() is %d",rslt);

}


int8_t  bme680_iic_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
  	u8 i=0;
	BME_IIC_Start();    // ?? IIC
	BME_IIC_Send_Byte(BME_IIC_ADDR);  // ???
	BME_IIC_Wait_Ack();
	BME_IIC_Send_Byte(reg_addr); 
	BME_IIC_Wait_Ack();
	BME_IIC_Stop();
	BME_IIC_Start();    // ?? IIC
	BME_IIC_Send_Byte(BME_IIC_ADDR|0x01); // ????
	BME_IIC_Wait_Ack();  // ????? ACK??
	len--;
	for(i= 0 ; i< len;i++) // ????????????????? ACKM
	{
			data[i] = BME_IIC_Read_Byte(1); //???? ACK?? ????
	}
	data[i] = BME_IIC_Read_Byte(0); //????? ACK?? ???????????
	BME_IIC_Stop();

  return 0;
}
int8_t  bme680_iic_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
  	u8 cnt=0;
	BME_IIC_Start();    // ?? IIC
	BME_IIC_Send_Byte(BME_IIC_ADDR);	
	BME_IIC_Wait_Ack();
	BME_IIC_Send_Byte(reg_addr);  
	BME_IIC_Wait_Ack();	  

	while(len--)
	{
		BME_IIC_Send_Byte(data[cnt]);
   	BME_IIC_Wait_Ack();	  
		cnt ++;
	}
	BME_IIC_Stop();
  return 0;
}
void Get_A_BME_Rreg(u8 reg_ADDR)
{
  u8 temp = 0xa5;
  BME_Read_registers(reg_ADDR,&temp,1);
  printf("\r\nREG 0x%2X: 0x%2X\r\n",reg_ADDR,temp);
  
}
void Set_A_BME_Rreg(u8 reg_ADDR,u8 Val)
{
  u8 temp = Val;
  BME_Write_registers(reg_ADDR,&temp,1);  
}

void Search_IIC(void)
{
  u8 i;
  for(i=0;i<0xff;i++)
  {
    	BME_IIC_Start();    // ?? IIC
	BME_IIC_Send_Byte(i);  // ???
	if(BME_IIC_Wait_Ack() == 0)
    printf( "ID : 0x%2X",i);
  }
}

void BME680_Init(void)
{  
	GPIO_InitTypeDef  GPIO_InitStructure;
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);//??GPIOA??
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);//??GPIOB??
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//??
  GPIO_Init(GPIOA, &GPIO_InitStructure);//???
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  GPIO_Init(GPIOB, &GPIO_InitStructure);//???
 //BME680_Reg_Init();
}



void BME_Write_registers(u8 Reg_ADDR,u8* data_buff,u8 len)
{
      u8 cnt=0;
      BME_IIC_Start();    // ?? IIC
      BME_IIC_Send_Byte(BME_IIC_ADDR);		
      BME_IIC_Wait_Ack();
      BME_IIC_Send_Byte(Reg_ADDR);
      BME_IIC_Wait_Ack();	  

      while(len--)
      {
          BME_IIC_Send_Byte(data_buff[cnt]);
          BME_IIC_Wait_Ack();	  
          cnt ++;
      }
      BME_IIC_Stop();
	
}

void BME_Read_registers(u8 Reg_ADDR,u8* data_buff,u8 len)
{
	u8 i=0;
	BME_IIC_Start();    // ?? IIC
	BME_IIC_Send_Byte(BME_IIC_ADDR);  // ???
	BME_IIC_Wait_Ack();
	BME_IIC_Send_Byte(Reg_ADDR); 
	BME_IIC_Wait_Ack();
	BME_IIC_Stop();
	BME_IIC_Start();    // ?? IIC
	BME_IIC_Send_Byte(BME_IIC_ADDR|0x01); // ????
	BME_IIC_Wait_Ack();  // ????? ACK??
	len--;
	for(i= 0 ; i< len;i++) // ????????????????? ACKM
	{
			data_buff[i] = BME_IIC_Read_Byte(1); //???? ACK?? ????
	}
	data_buff[i] = BME_IIC_Read_Byte(0); //????? ACK?? ???????????
	BME_IIC_Stop();
}


static void BME_IIC_Start(void)
{
   BME_SDA_OUT();
   BME_IIC_SDA=1;                    
   BME_IIC_SCL=1;
   delay_us(BME_IIC_Dealy);
   BME_IIC_SDA=0;//START:when CLK is high,DATA change form high to low 
   delay_us(BME_IIC_Dealy);
   BME_IIC_SCL=0;//??I2C??,????????? 
}          
static void BME_IIC_Stop(void)
{
  BME_SDA_OUT();
  BME_IIC_SCL=0;
  BME_IIC_SDA=0;//STOP:when CLK is high DATA change form low to high
  BME_IIC_SCL=1; 
  delay_us(BME_IIC_Dealy);
  BME_IIC_SDA=1;//??I2C??????
}
static u8   BME_IIC_Wait_Ack(void)
{
  u8 ucErrTime=0;
  BME_SDA_IN();      //SDA?????  
  BME_IIC_SDA=1;delay_us(BME_IIC_Dealy);           
  BME_IIC_SCL=1;delay_us(BME_IIC_Dealy);         
  while(BME_READ_SDA)
  {
     ucErrTime++;
     if(ucErrTime>10)
     {
         BME_IIC_Stop();
		//	 printf("No Ack");
         return 1;
     }
		 delay_us(1);
  }
  BME_IIC_SCL=0;//????0            
  return 0;  
} 
static void BME_IIC_Ack(void)
{
  BME_IIC_SCL=0;
  BME_SDA_OUT();
  BME_IIC_SDA=0;
  delay_us(BME_IIC_Dealy);
  BME_IIC_SCL=1;
  delay_us(BME_IIC_Dealy);
  BME_IIC_SCL=0;
}
static void BME_IIC_NAck(void)
{
   BME_IIC_SCL=0;
   BME_SDA_OUT();
   BME_IIC_SDA=1;
   delay_us(BME_IIC_Dealy);
   BME_IIC_SCL=1;
   delay_us(BME_IIC_Dealy);
   BME_IIC_SCL=0;
}                                                                              
static void BME_IIC_Send_Byte(u8 txd)
{                        
  u8 t;   
  BME_SDA_OUT();             
  BME_IIC_SCL=0;//??????????
  for(t=0;t<8;t++)
  {              
    BME_IIC_SDA=(txd&0x80)>>7;
    txd<<=1;           
    delay_us(BME_IIC_Dealy);   
    BME_IIC_SCL=1;
    delay_us(BME_IIC_Dealy); 
    BME_IIC_SCL=0;        
    delay_us(BME_IIC_Dealy);
  }         
}             
static u8 BME_IIC_Read_Byte(unsigned char ack)
{
  unsigned char i,receive=0;
  BME_SDA_IN();
  for(i=0;i<8;i++ )
  {
      BME_IIC_SCL=0; 
      delay_us(BME_IIC_Dealy);
      BME_IIC_SCL=1;
      receive<<=1;
      if(BME_READ_SDA)receive++;   
        delay_us(BME_IIC_Dealy); 
  }                                         
  if (!ack)
      BME_IIC_NAck();//nACK
  else
      BME_IIC_Ack(); //ACK   
  return receive;
}





